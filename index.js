var http = require('http');

var app = require('app');
var server = http.createServer(app.callback());

var config = require('config.json');
var conf = config.http || {};

server.listen(conf.port);

