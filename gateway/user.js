var validator = require('service/validator');
var apiToken = require('api/token');
var apiUser = require('api/user');

var user = module.exports = {};

user.createUser = function *(data) {
  yield validator(data, {
    type: 'object',
    properties: {
      email: { $ref: 'type:email' },
      password: { $ref: 'type:password' }
    },
    required: ['email', 'password'],
    additionalProperties: false
  });

  var user = yield apiUser.createUserByEmailPassword(data.email, data.password);
  if (!user) return null;

  return {
    token: (yield apiToken.createToken(user.id)),
    user: {
      id: user.id
    }
  };

};


user.getUserByToken = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  return {
    id: user.id,
    email: user.email
  }
};


user.getUserRolesByToken = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  return {
    roles: ['user']
  }
};