var validator = require('service/validator');
var apiToken = require('api/token');
var apiUser = require('api/user');
var apiBid = require('api/bid');

var bid = module.exports = {};

bid.createBid = function *(token, data) {
  yield validator(token, {
    $ref: 'type:token'
  });
  yield validator(data, {
    type: 'object',
    properties: {
      duration: { type: 'integer' },
      komi: { type: 'number' }
    },
    required: [],
    additionalProperties: false
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  return yield apiBid.createBid(user.id, data.duration, data.komi);
};

bid.getActiveBids = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  var bids = yield apiBid.getActiveBids();
  var ids = bids.map(function(bid) {
    return bid.user_fk;
  });

  var users = yield apiUser.getUsersById(ids);
  var usersMap = {};
  users.forEach(function(user) {
    usersMap[user.id] = user;
  });
  bids.forEach(function(bid) {
    var user = usersMap[bid.user_fk];
    bid.user = {
      id: user.id,
      email: user.email
    }
  });

  return bids;
};
