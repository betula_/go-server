var validator = require('service/validator');
var apiToken = require('api/token');
var apiUser = require('api/user');

var token = module.exports = {};

token.createToken = function *(data) {
  yield validator(data, {
    type: 'object',
    properties: {
      email: { $ref: 'type:email' },
      password: { $ref: 'type:password' }
    },
    required: ['email', 'password'],
    additionalProperties: false
  });

  var user = yield apiUser.getUserByEmailPassword(data.email, data.password);
  if (!user) return null;

  return {
    token: (yield apiToken.createToken(user.id)),
    user: {
      id: user.id
    }
  };
};

