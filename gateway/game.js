var validator = require('service/validator');
var apiToken = require('api/token');
var apiUser = require('api/user');
var apiBid = require('api/bid');
var apiGame = require('api/game');
var apiStep = require('api/step');
var apiStone = require('api/stone');
var apiGamer = require('api/gamer');

var game = module.exports = {};

game.createGame = function *(token, data) {
  yield validator(token, {
    $ref: 'type:token'
  });
  yield validator(data, {
    type: 'object',
    properties: {
      bid: { type: 'integer' }
    },
    required: ['bid'],
    additionalProperties: false
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  return yield apiGame.createGameById(user.id, data.bid);
};

game.getGames = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  var games = yield apiGame.getGamesByUserId(user.id);

  var gamesIds = games.map(function(game) {
    return game.id;
  });
  var gamers = yield apiGamer.getGamersByGamesId(gamesIds);
  var usersIds = gamers.map(function(gamer) {
    return gamer.user_fk;
  });
  var users = yield apiUser.getUsersById(usersIds);
  var usersMap = {};
  users.forEach(function(user) {
    usersMap[user.id] = {
      id: user.id,
      email: user.email,
      gamelose: user.gamelose,
      gamewin: user.gamewin,
      rating: user.rating
    }
  });
  var gamersMap = {};
  gamers.forEach(function(gamer) {
    gamer.user = usersMap[gamer.user_fk];
    if (!gamersMap[gamer.game_fk]) {
      gamersMap[gamer.game_fk] = [];
    }
    gamersMap[gamer.game_fk].push(gamer);
  });

  games.forEach(function(game) {
    game.gamers = gamersMap[game.id];
  });

  return games;
};

game.getActiveGameExpanded = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  var game = yield apiGame.getActiveGameByUserId(user.id);
  if (!game) return null;

  var gamers = yield apiGamer.getGamersByGameId(game.id);
  game.gamers = gamers;

  var usersIds = gamers.map(function(game) {
    return game.user_fk;
  });
  var users = yield apiUser.getUsersById(usersIds);
  var usersMap = {};
  users.forEach(function(user) {
    usersMap[user.id] = {
      id: user.id,
      email: user.email
    };
  });
  gamers.forEach(function(gamer) {
    gamer.user = usersMap[gamer.user_fk];
  });

  var steps = yield apiStep.getStepsByGameId(game.id);
  var stepsIds = steps.map(function(step) {
    return step.stone_fk;
  });
  var stones = yield apiStone.getStonesById(stepsIds);
  var stonesMap = {};
  stones.forEach(function(stone) {
    stonesMap[stone.id] = stone;
  });
  steps.forEach(function(step) {
    step.stone = stonesMap[step.stone_fk];
    step.user = usersMap[step.user_fk];
  });

  game.steps = steps;
  return game;

};

game.stepInActiveGame = function *(token, data) {
  yield validator(token, {
    $ref: 'type:token'
  });
  yield validator(data, {
    type: 'object',
    properties: {
      row: { type: 'integer' },
      col: { type: 'integer' }
    },
    required: ['row', 'col'],
    additionalProperties: false
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  var game = yield apiGame.getActiveGameByUserId(user.id);
  if (!game) return null;

  return yield apiStep.stepInGameIdByUserId(game.id, user.id, data.row, data.col);
};

game.passInActiveGame = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  var game = yield apiGame.getActiveGameByUserId(user.id);
  if (!game) return null;

  return yield apiStep.passInGameIdByUserId(game.id, user.id);
};

game.quitFromActiveGame = function *(token) {
  yield validator(token, {
    $ref: 'type:token'
  });

  var user = yield apiUser.getUserByToken(token);
  if (!user) return null;

  return yield apiGame.quitFromActiveGameByUserId(user.id);
};