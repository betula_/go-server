/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 90302
 Source Host           : localhost
 Source Database       : go-server
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90302
 File Encoding         : utf-8

 Date: 03/17/2014 00:43:20 AM
*/

-- ----------------------------
--  Sequence structure for bid_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bid_id_seq";
CREATE SEQUENCE "public"."bid_id_seq" INCREMENT 1 START 44 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."bid_id_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for game_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."game_id_seq";
CREATE SEQUENCE "public"."game_id_seq" INCREMENT 1 START 15 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."game_id_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for step_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."step_id_seq";
CREATE SEQUENCE "public"."step_id_seq" INCREMENT 1 START 165 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."step_id_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for stone_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."stone_id_seq";
CREATE SEQUENCE "public"."stone_id_seq" INCREMENT 1 START 151 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."stone_id_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_id_seq";
CREATE SEQUENCE "public"."user_id_seq" INCREMENT 1 START 12 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."user_id_seq" OWNER TO "postgres";

-- ----------------------------
--  Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."user";
CREATE TABLE "public"."user" (
	"id" int8 NOT NULL DEFAULT nextval('user_id_seq'::regclass),
	"email" varchar(255) NOT NULL COLLATE "default",
	"password" varchar(40) NOT NULL COLLATE "default",
	"gamewin" int4 NOT NULL DEFAULT 0,
	"gamelose" int4 NOT NULL DEFAULT 0,
	"rating" float8 NOT NULL DEFAULT 0
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."user" OWNER TO "postgres";

-- ----------------------------
--  Table structure for game
-- ----------------------------
DROP TABLE IF EXISTS "public"."game";
CREATE TABLE "public"."game" (
	"id" int8 NOT NULL DEFAULT nextval('game_id_seq'::regclass),
	"isclosed" bool NOT NULL DEFAULT false,
	"size" int2 NOT NULL DEFAULT 9,
	"bid_fk" int8 NOT NULL,
	"dtc" timestamp(6) NOT NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."game" OWNER TO "postgres";

-- ----------------------------
--  Table structure for gamer
-- ----------------------------
DROP TABLE IF EXISTS "public"."gamer";
CREATE TABLE "public"."gamer" (
	"user_fk" int8 NOT NULL,
	"game_fk" int8 NOT NULL,
	"iswhite" bool NOT NULL,
	"point" float4 NOT NULL DEFAULT 0
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."gamer" OWNER TO "postgres";

-- ----------------------------
--  Table structure for stone
-- ----------------------------
DROP TABLE IF EXISTS "public"."stone";
CREATE TABLE "public"."stone" (
	"id" int8 NOT NULL DEFAULT nextval('stone_id_seq'::regclass),
	"row" int2 NOT NULL,
	"col" int2 NOT NULL,
	"isdeath" bool NOT NULL DEFAULT false,
	"iswhite" bool NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."stone" OWNER TO "postgres";

-- ----------------------------
--  Table structure for step
-- ----------------------------
DROP TABLE IF EXISTS "public"."step";
CREATE TABLE "public"."step" (
	"id" int8 NOT NULL DEFAULT nextval('step_id_seq'::regclass),
	"time" timestamp(6) WITH TIME ZONE NOT NULL DEFAULT now(),
	"ispass" bool NOT NULL DEFAULT false,
	"game_fk" int8 NOT NULL,
	"user_fk" int8 NOT NULL,
	"stone_fk" int8
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."step" OWNER TO "postgres";

-- ----------------------------
--  Table structure for bid
-- ----------------------------
DROP TABLE IF EXISTS "public"."bid";
CREATE TABLE "public"."bid" (
	"id" int8 NOT NULL DEFAULT nextval('bid_id_seq'::regclass),
	"timecreate" timestamp(6) WITH TIME ZONE NOT NULL DEFAULT now(),
	"user_fk" int8 NOT NULL,
	"komi" float4 NOT NULL,
	"duration" int2 NOT NULL,
	"game_fk" int8,
	"aborted" bool NOT NULL DEFAULT false
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."bid" OWNER TO "postgres";


-- ----------------------------
--  Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."bid_id_seq" OWNED BY "bid"."id";
ALTER SEQUENCE "public"."game_id_seq" OWNED BY "game"."id";
ALTER SEQUENCE "public"."step_id_seq" OWNED BY "step"."id";
ALTER SEQUENCE "public"."stone_id_seq" OWNED BY "stone"."id";
ALTER SEQUENCE "public"."user_id_seq" OWNED BY "user"."id";
-- ----------------------------
--  Primary key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table user
-- ----------------------------
CREATE UNIQUE INDEX  "user_id_key" ON "public"."user" USING btree("id" ASC NULLS LAST);

-- ----------------------------
--  Triggers structure for table user
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16449" AFTER UPDATE ON "public"."user" FROM "public"."gamer" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16449" ON "public"."user" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16448" AFTER DELETE ON "public"."user" FROM "public"."gamer" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16448" ON "public"."user" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16418" AFTER UPDATE ON "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16418" ON "public"."user" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16417" AFTER DELETE ON "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16417" ON "public"."user" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16426" AFTER UPDATE ON "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16426" ON "public"."user" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16425" AFTER DELETE ON "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16425" ON "public"."user" IS NULL;

-- ----------------------------
--  Primary key structure for table game
-- ----------------------------
ALTER TABLE "public"."game" ADD CONSTRAINT "game_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table game
-- ----------------------------
CREATE UNIQUE INDEX  "game_id_key" ON "public"."game" USING btree("id" ASC NULLS LAST);

-- ----------------------------
--  Triggers structure for table game
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16454" AFTER UPDATE ON "public"."game" FROM "public"."gamer" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16454" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16453" AFTER DELETE ON "public"."game" FROM "public"."gamer" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16453" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16502" AFTER UPDATE ON "public"."game" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16502" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16501" AFTER DELETE ON "public"."game" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16501" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16414" AFTER UPDATE ON "public"."game" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16414" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16413" AFTER INSERT ON "public"."game" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16413" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16431" AFTER UPDATE ON "public"."game" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16431" ON "public"."game" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16430" AFTER DELETE ON "public"."game" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16430" ON "public"."game" IS NULL;

-- ----------------------------
--  Primary key structure for table gamer
-- ----------------------------
ALTER TABLE "public"."gamer" ADD CONSTRAINT "gamer_pkey" PRIMARY KEY ("user_fk", "game_fk") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Triggers structure for table gamer
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16451" AFTER UPDATE ON "public"."gamer" FROM "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16451" ON "public"."gamer" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16450" AFTER INSERT ON "public"."gamer" FROM "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16450" ON "public"."gamer" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16456" AFTER UPDATE ON "public"."gamer" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16456" ON "public"."gamer" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16455" AFTER INSERT ON "public"."gamer" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16455" ON "public"."gamer" IS NULL;

-- ----------------------------
--  Primary key structure for table stone
-- ----------------------------
ALTER TABLE "public"."stone" ADD CONSTRAINT "stone_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Triggers structure for table stone
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16436" AFTER UPDATE ON "public"."stone" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16436" ON "public"."stone" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16435" AFTER DELETE ON "public"."stone" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16435" ON "public"."stone" IS NULL;

-- ----------------------------
--  Primary key structure for table step
-- ----------------------------
ALTER TABLE "public"."step" ADD CONSTRAINT "step_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Triggers structure for table step
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16428" AFTER UPDATE ON "public"."step" FROM "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16428" ON "public"."step" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16427" AFTER INSERT ON "public"."step" FROM "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16427" ON "public"."step" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16438" AFTER UPDATE ON "public"."step" FROM "public"."stone" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16438" ON "public"."step" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16437" AFTER INSERT ON "public"."step" FROM "public"."stone" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16437" ON "public"."step" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16433" AFTER UPDATE ON "public"."step" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16433" ON "public"."step" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16432" AFTER INSERT ON "public"."step" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16432" ON "public"."step" IS NULL;

-- ----------------------------
--  Primary key structure for table bid
-- ----------------------------
ALTER TABLE "public"."bid" ADD CONSTRAINT "bid_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table bid
-- ----------------------------
CREATE UNIQUE INDEX  "bid_id_key" ON "public"."bid" USING btree("id" ASC NULLS LAST);

-- ----------------------------
--  Triggers structure for table bid
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16420" AFTER UPDATE ON "public"."bid" FROM "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16420" ON "public"."bid" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16419" AFTER INSERT ON "public"."bid" FROM "public"."user" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16419" ON "public"."bid" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16504" AFTER UPDATE ON "public"."bid" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16504" ON "public"."bid" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_c_16503" AFTER INSERT ON "public"."bid" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_c_16503" ON "public"."bid" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16412" AFTER UPDATE ON "public"."bid" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16412" ON "public"."bid" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_a_16411" AFTER DELETE ON "public"."bid" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_a_16411" ON "public"."bid" IS NULL;

-- ----------------------------
--  Foreign keys structure for table game
-- ----------------------------
ALTER TABLE "public"."game" ADD CONSTRAINT "bid" FOREIGN KEY ("bid_fk") REFERENCES "public"."bid" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table gamer
-- ----------------------------
ALTER TABLE "public"."gamer" ADD CONSTRAINT "user" FOREIGN KEY ("user_fk") REFERENCES "public"."user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."gamer" ADD CONSTRAINT "game" FOREIGN KEY ("game_fk") REFERENCES "public"."game" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table step
-- ----------------------------
ALTER TABLE "public"."step" ADD CONSTRAINT "user" FOREIGN KEY ("user_fk") REFERENCES "public"."user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."step" ADD CONSTRAINT "stone" FOREIGN KEY ("stone_fk") REFERENCES "public"."stone" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."step" ADD CONSTRAINT "game" FOREIGN KEY ("game_fk") REFERENCES "public"."game" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table bid
-- ----------------------------
ALTER TABLE "public"."bid" ADD CONSTRAINT "user" FOREIGN KEY ("user_fk") REFERENCES "public"."user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."bid" ADD CONSTRAINT "game" FOREIGN KEY ("game_fk") REFERENCES "public"."game" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

