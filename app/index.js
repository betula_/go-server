var koa = require('koa');


var app = module.exports = koa();

app.context(require('koa-body-parser'));

app.use(function(next) {
  return function *() {
    console.log('\033[90m%s\033[0m', new Date().toISOString());
    yield next;
  }
});

app.use(require('koa-logger')());
app.use(require('koa-response-time')());

app.use(function(next) {
  return function *() {
    try {
      yield next;
    }
    catch (err) {

      this.status = err.status || 500;
      this.body = {
        status: 'Error',
        message: err.message
      };

      this.app.emit('error', err, this);
    }
  }
});

app.on('error', function(err){
  console.error('Sent error %s to the cloud', err.message);
});


app.use(function(next) {
  return function *() {
    this.set('Access-Control-Allow-Origin', '*');
    this.set('Access-Control-Allow-Headers', 'Accept, Origin, Authorization, Content-Type');
    this.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
    this.set('Access-Control-Max-Age', '1728000');
    if (this.method == 'OPTIONS') {
      this.status = 200;
      this.type = 'text';
      this.body = '';
    }
    else {
      yield next;
    }
  }
});


app.use(require('./router'));

app.use(function() {
  return function *() {
    this.status = 404;
    this.type = 'json';
    this.body = {
      status: 'NotFound',
      message: 'Not found'
    };
  }
});


