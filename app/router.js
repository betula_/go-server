var gatewayToken = require('gateway/token');
var gatewayUser = require('gateway/user');
var gatewayBid = require('gateway/bid');
var gatewayGame = require('gateway/game');

module.exports = router;

function router(next) {
  return function *() {
    var ctx = this;
    var post = function(path) { return ctx.method == 'POST' && ctx.path == path; };
    var get = function(path) { return ctx.method == 'GET' && ctx.path == path; };

    if (post('/token')) {
      // email, password
      this.body = yield gatewayToken.createToken(yield this.json);
    }

    if (post('/user')) {
      // email, password
      this.body = yield gatewayUser.createUser(yield this.json);
    }
    if (get('/user')) {
      // email, password
      this.body = yield gatewayUser.getUserByToken(this.get('Authorization'));
    }

    if (get('/user/roles')) {
      this.body = yield gatewayUser.getUserRolesByToken(this.get('Authorization'));
    }


    if (post('/bid')) {
      this.body = yield gatewayBid.createBid(this.get('Authorization'), (yield this.json) || {});
    }
    if (get('/bids')) {
      this.body = yield gatewayBid.getActiveBids(this.get('Authorization'));
    }

    if (post('/game')) {
      this.body = yield gatewayGame.createGame(this.get('Authorization'), yield this.json);
    }
    if (get('/game/active')) {
      this.body = yield gatewayGame.getActiveGameExpanded(this.get('Authorization'));
    }
    if (post('/game/active/step')) {
      this.body = yield gatewayGame.stepInActiveGame(this.get('Authorization'), yield this.json);
    }
    if (post('/game/active/step/pass')) {
      this.body = yield gatewayGame.passInActiveGame(this.get('Authorization'));
    }
    if (post('/game/active/quit')) {
      this.body = yield gatewayGame.quitFromActiveGame(this.get('Authorization'));
    }

    if (get('/games')) {
      this.body = yield gatewayGame.getGames(this.get('Authorization'));
    }



    if (!this.body) {
      yield next;
    }
  }
}


