var pg = require('service/pg');

var game = module.exports = {};

game.createGameById = function *(userId, bidId) {

  // Check bit for BidId
  var bid = (yield pg.query('SELECT * FROM "bid" WHERE id = $1 AND user_fk != $2 AND game_fk IS NULL AND EXTRACT(epoch FROM timecreate) + duration > $3 AND NOT aborted', [
    bidId, userId, Date.now() / 1000
  ])).rows[0];
  if (!bid) return null;

  // Check user already in game
  var games = yield this.getActiveGamesByUsersId([userId, bid.user_fk]);
  if (games.length) return null;

  // Create game
  var game = (yield pg.query('INSERT INTO "game" (bid_fk) VALUES ($1) RETURNING id;', [
    bid.id
  ])).rows[0];
  if (!game) return null;

  // Abort active bit owned userId
  yield pg.query('UPDATE "bid" SET (aborted) = (TRUE) WHERE user_fk = $1 AND game_fk IS NULL AND EXTRACT(epoch FROM timecreate) + duration > $2 AND NOT aborted', [
    userId, Date.now() / 1000
  ]);

  // Set game to BitId
  yield pg.query('UPDATE "bid" SET (game_fk) = ($2) WHERE id = $1', [bid.id, game.id]);

  // Create two gamers
  var white = Math.random() < 0.5;
  yield pg.query('INSERT INTO "gamer" (user_fk, game_fk, iswhite) VALUES ($1, $2, $3)', [
    userId, game.id, white
  ]);
  yield pg.query('INSERT INTO "gamer" (user_fk, game_fk, iswhite) VALUES ($1, $2, $3)', [
    bid.user_fk, game.id, !white
  ]);

  return game;
};

game.getGameById = function *(id) {
  return (yield pg.query('SELECT * FROM "game" WHERE id = $1;', [id])).rows[0];
};

game.getActiveGameByUserId = function *(userId) {
  return (yield pg.query('SELECT game.* FROM game, gamer WHERE NOT game.isclosed AND game."id" = gamer.game_fk AND gamer.user_fk = $1 LIMIT 1;', [
    userId
  ])).rows[0];
};

game.getActiveGamesByUsersId = function *(userIds) {
  for (var i = 0, placeholder = []; i < userIds.length; i++) {
    placeholder.push('$'+(i+1));
  }
  return (yield pg.query('SELECT game.* FROM game, gamer WHERE NOT game.isclosed AND game."id" = gamer.game_fk AND gamer.user_fk IN ('+placeholder.join()+') LIMIT 1;',
    userIds
  )).rows;
};

game.getGamesByUserId = function *(userId) {
  return (yield pg.query('SELECT game.* FROM game, gamer WHERE game."id" = gamer.game_fk AND gamer.user_fk = $1 ORDER BY dtc DESC;', [
    userId
  ])).rows;
};

game.quitFromActiveGameByUserId = function *(userId) {
  var game = yield this.getActiveGameByUserId(userId);
  if (!game) return null;

  return yield this.gameClose(game.id);
};

game.gameClose = function *(gameId) {
  var game = (yield pg.query('SELECT * FROM game WHERE id = $1 AND NOT isclosed;', [gameId])).rows[0];
  if (!game) return null;

  yield pg.query('UPDATE game SET (isclosed) = (TRUE) WHERE id = $1', [gameId]);
  var gamers = (yield pg.query('SELECT * FROM gamer WHERE game_fk = $1', [gameId])).rows;

  var i;
  for (i = 0; i < gamers.length; i++) {
    gamers[1-i].point = (yield pg.query('SELECT COUNT(*) AS "point" FROM step, stone WHERE stone.id = step.stone_fk AND stone.isdeath AND step.game_fk = $1 AND step.user_fk = $2', [
      game.id,
      gamers[i].user_fk
    ])).rows[0].point;
  }
  for (i = 0; i < gamers.length; i++) {
    yield pg.query('UPDATE gamer SET (point) = ($3) WHERE game_fk = $1 AND user_fk = $2', [gameId, gamers[i].user_fk, gamers[i].point]);
  }

  if (gamers[0].point > gamers[1].point) {
    yield pg.query('UPDATE "user" SET (gamewin) = (gamewin+1) WHERE id = $1', [gamers[0].user_fk]);
    yield pg.query('UPDATE "user" SET (gamelose) = (gamelose+1) WHERE id = $1', [gamers[1].user_fk]);
  }
  if (gamers[1].point > gamers[0].point) {
    yield pg.query('UPDATE "user" SET (gamewin) = (gamewin+1) WHERE id = $1', [gamers[1].user_fk]);
    yield pg.query('UPDATE "user" SET (gamelose) = (gamelose+1) WHERE id = $1', [gamers[0].user_fk]);
  }

  var rating;
  for (i = 0; i < gamers.length; i++) {
    rating = (yield pg.query('SELECT SUM(point) AS "rating" FROM gamer WHERE user_fk = $1', [
      gamers[i].user_fk
    ])).rows[0].rating;
    yield pg.query('UPDATE "user" SET (rating) = ($2) WHERE id = $1', [gamers[i].user_fk, rating]);
  }

  return {
    id: gameId
  }

};
