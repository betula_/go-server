var pg = require('service/pg');

var gamer = module.exports = {};

gamer.getGamersByGameId = function *(gameId) {
  return (yield pg.query('SELECT * FROM gamer WHERE game_fk = $1;', [gameId])).rows;
};

gamer.getGamersByGamesId = function *(gameIds) {
  if (!gameIds.length) return [];
  for (var i = 0, placeholder = []; i < gameIds.length; i++) {
    placeholder.push('$'+(i+1));
  }

  return (yield pg.query('SELECT * FROM "gamer" WHERE game_fk IN ('+placeholder.join()+');', gameIds)).rows;
};