var pg = require('service/pg');
var apiGame = require('api/game');

var bid = module.exports = {};

bid.getActiveBids = function *() {
  return (yield pg.query('SELECT * FROM "bid" WHERE game_fk IS NULL AND EXTRACT(epoch FROM  timecreate) + duration > $1 AND NOT aborted ;', [
    Date.now() / 1000
  ])).rows;
};

bid.createBid = function *(userId, duration, komi) {
  duration = duration || 600;
  komi = komi || 7.5;

  var bid = (yield pg.query('SELECT * FROM "bid" WHERE user_fk = $1 AND game_fk IS NULL AND EXTRACT(epoch FROM timecreate) + duration > $2 AND NOT aborted;', [
    userId,
    Date.now() / 1000
  ])).rows[0];
  if (bid) return null;

  var game = yield apiGame.getActiveGameByUserId(userId);
  if (game) return null;

  return (yield pg.query('INSERT INTO "bid" (user_fk, duration, komi) VALUES ($1, $2, $3) RETURNING id;', [
    userId, duration, komi
  ])).rows[0];
};

bid.getBidById = function *(id) {
  return (yield pg.query('SELECT * FROM "bid" WHERE id = $1;', [id])).rows[0];
};



