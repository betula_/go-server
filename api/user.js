var pg = require('service/pg');
var apiToken = require('api/token');

var user = module.exports = {};

user.createUserByEmailPassword = function *(email, password) {
  var user = (yield pg.query('SELECT * FROM "user" WHERE email = $1;', [email])).rows[0];
  if (user) return null;

  user = (yield pg.query(
    'INSERT INTO "user" (email, password) VALUES ($1, $2) RETURNING id;',
    [
      email,
      pg.Client.md5(password)
    ]
  )).rows[0];

  return user;
};


user.getUserByEmailPassword = function *(email, password) {
  var user = (yield pg.query('SELECT * FROM "user" WHERE email = $1;', [email])).rows[0];
  if (!user) return null;

  var hash = pg.Client.md5(password);
  if (hash !== user.password) return null;

  return user;
};

user.getUserById = function *(id) {
  return (yield pg.query('SELECT * FROM "user" WHERE id = $1;', [id])).rows[0];
};

user.getUserByToken = function *(token) {
  var id = yield apiToken.resolveToken(token);
  if (!id) return null;
  return (yield this.getUserById(id));
};

user.getUsersById = function *(ids) {
  if (!ids.length) return [];
  for (var i = 0, placeholder = []; i < ids.length; i++) {
    placeholder.push('$'+(i+1));
  }
  return (yield pg.query('SELECT * FROM "user" WHERE id IN ('+placeholder.join()+');', ids)).rows;
};



