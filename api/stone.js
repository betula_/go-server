var pg = require('service/pg');

var stone = module.exports = {};

stone.getStonesById = function *(ids) {
  if (!ids.length) return [];
  for (var i = 0, placeholder = []; i < ids.length; i++) {
    placeholder.push('$'+(i+1));
  }

  return (yield pg.query('SELECT * FROM "stone" WHERE id IN ('+placeholder.join()+');', ids)).rows;
};





