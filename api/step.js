var pg = require('service/pg');
var apiGame = require('api/game');

var step = module.exports = {};

step.getStepsById = function *(ids) {
  if (!ids.length) return [];
  for (var i = 0, placeholder = []; i < ids.length; i++) {
    placeholder.push('$'+(i+1));
  }

  return (yield pg.query('SELECT * FROM "step" WHERE id IN ('+placeholder.join()+');', ids)).rows;
};

step.getStepsByGameId = function *(gameId) {
  return (yield pg.query('SELECT * FROM "step" WHERE game_fk = $1 ORDER BY "time" ASC', [gameId])).rows;
};

step.passInGameIdByUserId = function *(gameId, userId) {
  //Check active game
  var game = (yield pg.query('SELECT * FROM game WHERE id = $1 AND NOT isclosed;', [gameId])).rows[0];
  if (!game) return null;

  //Get gamer for userId
  var player = (yield pg.query('SELECT * FROM gamer WHERE game_fk = $1 AND user_fk = $2;', [gameId, userId])).rows[0];
  if (!player) return null;

  //Get last step gamer
  var last = (yield pg.query('SELECT * FROM step WHERE game_fk = $1 ORDER BY "time" DESC LIMIT 1;', [gameId])).rows[0];
  if (last) {
    if (last.user_fk == userId) { // If previous step is mine too
      return null;
    }
  }
  else {
    if (!player.iswhite) { // First step must be white
      return null;
    }
  }

  //If two pass one after one
  if (!last.stone_fk) {
    yield apiGame.gameClose(game.id);
  }

  //Pass step
  var step = (yield pg.query('INSERT INTO "step" (game_fk, user_fk, ispass) VALUES ($1, $2, TRUE) RETURNING id;', [gameId, userId])).rows[0];

  //Finish
  return step;
};

step.stepInGameIdByUserId = function *(gameId, userId, row, col) {

  //Check active game
  var game = (yield pg.query('SELECT * FROM game WHERE id = $1 AND NOT isclosed;', [gameId])).rows[0];
  if (!game) return null;

  //Check row and col values
  if (Math.max(row, col) >= game.size || Math.min(row, col) < 0) {
    return null;
  }

  //Get gamer for userId
  var player = (yield pg.query('SELECT * FROM gamer WHERE game_fk = $1 AND user_fk = $2;', [gameId, userId])).rows[0];
  if (!player) return null;

  //Get last step gamer
  var last = (yield pg.query('SELECT * FROM step WHERE game_fk = $1 ORDER BY "time" DESC LIMIT 1;', [gameId])).rows[0];
  if (last) {
    if (last.user_fk == userId) { // If previous step is mine too
      return null;
    }
  }
  else {
    if (!player.iswhite) { // First step must be white
      return null;
    }
  }

  // Create board
  var stones = (yield pg.query('SELECT step.*, stone.row, stone.col FROM step, stone WHERE stone.id=step.stone_fk AND NOT stone.isdeath AND step.game_fk=$1;', [gameId])).rows;
  var board = [];
  var i;
  for (i = 0; i < game.size; i++) {
    board.push(new Array(game.size));
  }
  stones.forEach(function(stone) {
    board[stone.row][stone.col] = stone.user_fk;
  });

  //Check stone in row, col position
  if (board[row][col]) return null;
  board[row][col] = userId;

  //Directions
  var d4 = [ [-1,0], [0,1], [1,0], [0,-1] ];

  //Get all of opponent groups around
  var groups = [];
  d4.forEach(function(k) {
    var p = [ row+k[0], col+k[1] ];
    var m = (board[p[0]] || [])[p[1]];
    if (typeof m !== 'undefined' && m !== userId) {
      if (!isInGroups(p, groups)) {
        groups.push(getGroup(board, p));
      }
    }
  });

  //Get power of opponent groups
  for (i = 0; i < groups.length; i++) {
    yield (function *(group) {
      if (!group.length) return;
      var power = getGroupPower(board, group);
      if (!power) {

        //Make death all stones in group
        var parts = [];
        var values = [gameId];
        var index = 1;
        group.forEach(function(v) {
          parts.push('(stone."row" = $'+(++index)+' AND stone."col" = $'+(++index)+')');
          values.push(v[0], v[1]);
        });
        var stones = (yield pg.query('SELECT stone.* FROM step, stone WHERE stone.id=step.stone_fk AND NOT stone.isdeath AND step.game_fk=$1 AND ('+parts.join('OR')+');', values)).rows;
        var ids = stones.map(function(stone) {
          return stone.id;
        });
        if (ids.length) {
          for (var i = 0, placeholder = []; i < ids.length; i++) {
            placeholder.push('$'+(i+1));
          }
          yield pg.query('UPDATE stone SET (isdeath) = (TRUE) WHERE id IN ('+placeholder.join()+')', ids);

          //Delete all stones from group from board
          group.forEach(function(v) {
            delete board[v[0]][v[1]];
          });
        }
      }
    })(groups[i]);
  }

  //if after the death of a group, all the stones around the position alive and enemies cancel step
  var notEnemies = 0;
  d4.forEach(function(k) {
    var r = row+k[0];
    var c = col+k[1];
    if (board.length > r && r >= 0) {
      var line = board[r];
      if (line.length > c && c >= 0) {
        var m = line[c];
        if (typeof m == 'undefined' || m == userId) {
          notEnemies ++;
        }
      }
    }
  });
  if (!notEnemies) {
    return null;
  }

  // Create stone and step
  var stone = (yield pg.query('INSERT INTO "stone" (row, col, iswhite) VALUES ($1, $2, $3) RETURNING id;', [row, col, player.iswhite])).rows[0];
  var step = (yield pg.query('INSERT INTO "step" (game_fk, user_fk, stone_fk) VALUES ($1, $2, $3) RETURNING id;', [gameId, userId, stone.id])).rows[0];

  // Finish
  return step;



  //Get group of stones
  function getGroup(board, point) {
    var kind = board[point[0]][point[1]];
    if (!kind) return [];

    var queue = [ [point[0], point[1]] ];
    var queueHash = {};
    queueHash[queue[0].join()] = queue[0];
    var cursor = 0;

    var d = [ [-1,0], [0,1], [1,0], [0,-1] ];
    var k, p, m, key;
    while (cursor < queue.length) {
      p = queue[cursor++];

      for (k = 0; k < d.length; k++) {
        m = [ p[0]+d[k][0], p[1]+d[k][1] ];
        if ( (board[m[0]] || [])[m[1]] === kind ) {
          key = m.join();
          if (!queueHash[key]) {
            queue.push(m);
            queueHash[key] = m;
          }
        }
      }

    }
    return queue;
  }

  //Get power of group
  function getGroupPower(board, group) {
    if (!group.length) return 0;
    var kind = board[group[0][0]][group[0][1]];
    if (!kind) return 0;

    var powerHash = {};
    var power = [];
    var d = [ [-1,0], [0,1], [1,0], [0,-1] ];
    var p, m, i, k, key;
    for (i = 0; i < group.length; i++) {
      p = group[i];
      for (k = 0; k < d.length; k++) {
        m = [ p[0]+d[k][0], p[1]+d[k][1] ];
        if (Math.max(m[0],m[1]) < board.length && Math.min(m[0],m[1]) >= 0) {
          if (!board[m[0]][m[1]]) {
            key = m.join();
            if (!powerHash[key]) {
              power.push(m);
              powerHash[key] = m;
            }
          }
        }
      }
    }
    return power.length;
  }

  function isInGroup(point, group) {
    var i, p;
    for (i = 0; i < group.length; i++) {
      p = group[i];
      if (point[0] == p[0] && point[1] == p[1]) {
        return true;
      }
    }
    return false;
  }

  function isInGroups(point, groups) {
    var i;
    for (i = 0; i < groups.length; i++) {
      if (isInGroup(point, groups[i])) {
        return true;
      }
    }
    return false;
  }



};





