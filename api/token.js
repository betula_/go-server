var uniq = require('uniq');
var redis = require('service/redis');

var token = module.exports = {};

token.createToken = function *(id, ttl) {
  ttl = ttl || 3600*24*77;
  var token = uniq();
  yield redis.setex(token, ttl, JSON.stringify(id));
  return token;
};

token.resolveToken = function *(token) {
  var data = yield redis.get(token);
  return JSON.parse(data);
};

token.freeToken = function *(token) {
  return yield redis.del(token);
};


