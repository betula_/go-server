var co = require('co');
var pg = require('co-pg');
var config = require('service/config');
var debug = require('debug')('go:service/pg');
var util = require('util');

var conf = config.pg || {};

module.exports = {
  query: function() {
    var pg = this;
    var args = [].slice.call(arguments);
    return co(function *() {
      var client = yield pg.connect();
      try {
        debug(args);
        var result = yield client.query.apply(client, args);
        debug(JSON.stringify(result.rows[0] || null));
      }
      catch(err) {
        debug('!');
        throw err;
      }
      finally {
        client.free();
      }
      return result;
    });
  },
  connect: function() {
    return pg.connect(conf.url);
  },
  Client: pg.Client,
  get pools() {
    return pg.pools
  }
};
