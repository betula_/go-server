var tv4 = require('tv4').tv4;
var schema = tv4.addSchema;

schema({
  id: 'type:email',
  type: 'string',
  //Validator from https://github.com/chriso/node-validator/blob/master/lib/validators.js
  pattern: /^(?:[\w\!\\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/,
  maxLength: 254
});

schema({
  id: 'type:url',
  type: 'string',
  //Validator from https://github.com/chriso/node-validator/blob/master/lib/validators.js
  pattern: /^(?!mailto:)(?:(?:https?|ftp):\/\/)?(?:\S+(?::\S*)?@)?(?:(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[0-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))|localhost)(?::\d{2,5})?(?:\/[^\s]*)?$/i,
  maxLength: 2082
});

schema({
  id: 'type:date/ISO',
  type: 'string',
  pattern: /^(\d{4})-?(\d{2})-?(\d{2})[Tt ](\d{2}):?(\d{2}):?(\d{2})(\.\d+)?([Zz]|(?:([+-])(\d{2}):?(\d{2})))$/
});

schema({
  id: 'type:date',
  oneOf: [
    { $ref: 'type:date/ISO' }
  ]
});

schema({
  id: 'type:password',
  type: 'string',
  minLength: 5,
  maxLength: 40
});