var tv4 = require('tv4').tv4;
var schema = tv4.addSchema;

schema({
  id: 'type:token',
  type: 'string',
  pattern: '^[-_=A-Za-z0-9]*$',
  minLength: 24,
  maxLength: 24
});