var tv4 = require('tv4').tv4;

module.exports = validator;

function *validator(data, schema) {
  var result = tv4.validateMultiple(data, schema);
  if (result.missing.length) {
    throw new Error('Validator schema missing: ' + result.missing.join());
  }
  if (!result.valid) {
    var messages = result.errors.map(function(error) {
      return '(' + error.dataPath + ': ' + error.message + ')';
    });
    throw new Error(messages.join(', '));
  }
}