var redis = require('co-redis');
var config = require('service/config');

var conf = config.redis || {};
var client = redis.createClient(conf.port, conf.host);

module.exports = client;
